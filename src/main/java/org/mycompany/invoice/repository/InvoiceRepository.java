package org.mycompany.invoice.repository;

import org.mycompany.invoice.entity.Invoice;

import java.util.ArrayList;
import java.util.List;

public class InvoiceRepository implements InvoiceRepositoryInterface {

    private static List<Invoice> invoices = new ArrayList<>();

    /**
     * Add new invoice in a list (invoices)
     * @param invoice Invoice
     */
    public void create(Invoice invoice) {
        invoices.add(invoice);
        System.out.println("Invoice N°" + invoice.getNumber() + " created for customer " + invoice.getCustomerName());
    }


}
