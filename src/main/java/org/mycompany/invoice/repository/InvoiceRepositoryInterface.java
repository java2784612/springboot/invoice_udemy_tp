package org.mycompany.invoice.repository;

import org.mycompany.invoice.entity.Invoice;

public interface InvoiceRepositoryInterface {

    void create(Invoice invoice);
}

