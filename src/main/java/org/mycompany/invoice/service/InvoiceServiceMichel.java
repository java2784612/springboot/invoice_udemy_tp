package org.mycompany.invoice.service;

import org.mycompany.invoice.entity.Invoice;
import org.mycompany.invoice.repository.InvoiceRepositoryInterface;

public class InvoiceServiceMichel implements InvoiceServiceInterface {

    private static long lastNumber = 112L;

    private InvoiceRepositoryInterface invoiceRepository;

    public InvoiceRepositoryInterface getInvoiceRepository() {
        return invoiceRepository;
    }

    public void setInvoiceRepository(InvoiceRepositoryInterface invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    public void createInvoice(Invoice invoice) {
        invoice.setNumber("INT_" + (++lastNumber));
        invoiceRepository.create(invoice);
    }
}
