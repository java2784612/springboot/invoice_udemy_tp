package org.mycompany.invoice.service;

import org.mycompany.invoice.entity.Invoice;

public interface InvoiceServiceInterface {
    void createInvoice(Invoice invoice);
}
