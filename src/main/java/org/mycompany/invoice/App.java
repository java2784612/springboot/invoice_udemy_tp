package org.mycompany.invoice;

import org.mycompany.invoice.controller.InvoiceController;
import org.mycompany.invoice.controller.InvoiceControllerMichel;
import org.mycompany.invoice.repository.InvoiceRepository;
import org.mycompany.invoice.repository.InvoiceRepositoryMichel;
import org.mycompany.invoice.service.InvoiceService;
import org.mycompany.invoice.service.InvoiceServiceMichel;

import java.util.Scanner;


public class App
{
    public static void main( String[] args )
    {
        System.out.println("Select your Invoice Configuration : ");
        Scanner sc = new Scanner(System.in);
        String customerConfig = sc.nextLine();

        // Verify Invoice Configuration
        if (customerConfig.equals("1")) {
            InvoiceController invoiceController = new InvoiceController();
            InvoiceService invoiceService = new InvoiceService();
            invoiceController.setInvoiceService(invoiceService);
            InvoiceRepository invoiceRepository = new InvoiceRepository();
            invoiceService.setInvoiceRepository(invoiceRepository);
            invoiceController.createInvoice();
        }
        else if (customerConfig.equals("2")) {
            InvoiceControllerMichel invoiceController = new InvoiceControllerMichel();
            InvoiceServiceMichel invoiceService = new InvoiceServiceMichel();
            invoiceController.setInvoiceService(invoiceService);
            InvoiceRepositoryMichel invoiceRepository = new InvoiceRepositoryMichel();
            invoiceService.setInvoiceRepository(invoiceRepository);
            invoiceController.createInvoice();
        }
    }
}
